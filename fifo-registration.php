<?php
/*
Plugin Name: Fifo Registration
Plugin URI: http://fifo.com.bd
Description: Provides simple registration and store the data into another DB - must be needed to configure with hard coding.. shortcode [register_form], [login_form]
Version: 1.0
Author: FIFO
Author URI: http://fifo.com.bd
*/
// adding custom wp user meta
function fifo_add_custom_user_profile_fields( $user ) {
?>
	<h3><?php _e('Extra Profile Information', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="company_name"><?php _e('Company Name', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="company_name" id="company_name" value="<?php echo esc_attr( get_the_author_meta( 'company_name', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your company name.', 'your_textdomain'); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="fifo_title"><?php _e('Title', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="fifo_title" id="fifo_title" value="<?php echo esc_attr( get_the_author_meta( 'fifo_title', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your title.', 'your_textdomain'); ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="fifo_country"><?php _e('Country', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="fifo_country" id="fifo_country" value="<?php echo esc_attr( get_the_author_meta( 'fifo_country', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your Country.', 'your_textdomain'); ?></span>
			</td>
		</tr>				
	</table>
<?php }

function fifo_save_custom_user_profile_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	
	update_usermeta( $user_id, 'company_name', $_POST['company_name'] );
	update_usermeta( $user_id, 'fifo_title', $_POST['fifo_title'] );
	update_usermeta( $user_id, 'fifo_country', $_POST['fifo_country'] );	
}

add_action( 'show_user_profile', 'fifo_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'fifo_add_custom_user_profile_fields' );

add_action( 'personal_options_update', 'fifo_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'fifo_save_custom_user_profile_fields' );


// user registration login form
function fifo_registration_form() {
 
	// only show the registration form to non-logged-in members
	if(!is_user_logged_in()) {
 
		global $fifo_load_css;
 
		// set this to true so the CSS is loaded
		$fifo_load_css = true;
 
		// check to make sure user registration is enabled
		$registration_enabled = get_option('users_can_register');
 
		// only show the registration form if allowed
		if($registration_enabled) {
			$output = fifo_registration_form_fields();
		} else {
			$output = __('User registration is not enabled');
		}
		return $output;
	}
}
add_shortcode('register_form', 'fifo_registration_form');
// user login form
function fifo_login_form() {
 
	if(!is_user_logged_in()) {
 
		global $fifo_load_css;
 
		// set this to true so the CSS is loaded
		$fifo_load_css = true;
 
		$output = fifo_login_form_fields();
	} else {
		// could show some logged in user info here
		// $output = 'user info here';
	}
	return $output;
}
add_shortcode('login_form', 'fifo_login_form');
// registration form fields
function fifo_registration_form_fields() {
 
	ob_start(); ?>	
		<h3 class="fifo_header"><?php _e('Register New Account'); ?></h3>
 
		<?php 
		// show any error messages after form submission
		fifo_show_error_messages(); ?>
 
		<form id="fifo_registration_form" class="fifo_form" action="" method="POST">
			<fieldset>
				<p>
					<label for="fifo_user_Login"><?php _e('Username'); ?></label>
					<input name="fifo_user_login" id="fifo_user_login" class="required" type="text"/>
				</p>
				<p>
					<label for="fifo_user_email"><?php _e('Email'); ?></label>
					<input name="fifo_user_email" id="fifo_user_email" class="required" type="email"/>
				</p>
				<p>
					<label for="fifo_user_first"><?php _e('First Name'); ?></label>
					<input name="fifo_user_first" id="fifo_user_first" type="text"/>
				</p>
				<p>
					<label for="fifo_user_last"><?php _e('Last Name'); ?></label>
					<input name="fifo_user_last" id="fifo_user_last" type="text"/>
				</p>
				<p>
					<label for="company_name"><?php _e('Company Name'); ?></label>
					<input name="company_name" id="company_name" type="text"/>
				</p>
				<p>
					<label for="fifo_title"><?php _e('Title'); ?></label>
					<input name="fifo_title" id="fifo_title" type="text"/>
				</p>
				<p>
					<label for="fifo_country"><?php _e('Country'); ?></label>
					<input name="fifo_country" id="fifo_country" type="text"/>
				</p>												
				<p>
					<label for="password"><?php _e('Password'); ?></label>
					<input name="fifo_user_pass" id="password" class="required" type="password"/>
				</p>
				<p>
					<label for="password_again"><?php _e('Password Again'); ?></label>
					<input name="fifo_user_pass_confirm" id="password_again" class="required" type="password"/>
				</p>
				<p>
					<input type="hidden" name="fifo_register_nonce" value="<?php echo wp_create_nonce('fifo-register-nonce'); ?>"/>
					<input type="submit" value="<?php _e('Register Your Account'); ?>"/>
				</p>
			</fieldset>
		</form>
	<?php
	return ob_get_clean();
}
// login form fields
function fifo_login_form_fields() {
 
	ob_start(); ?>
		<h3 class="fifo_header"><?php _e('Login'); ?></h3>
 
		<?php
		// show any error messages after form submission
		fifo_show_error_messages(); ?>
 
		<form id="fifo_login_form"  class="fifo_form"action="" method="post">
			<fieldset>
				<p>
					<label for="fifo_user_Login">Username</label>
					<input name="fifo_user_login" id="fifo_user_login" class="required" type="text"/>
				</p>
				<p>
					<label for="fifo_user_pass">Password</label>
					<input name="fifo_user_pass" id="fifo_user_pass" class="required" type="password"/>
				</p>
				<p>
					<input type="hidden" name="fifo_login_nonce" value="<?php echo wp_create_nonce('fifo-login-nonce'); ?>"/>
					<input id="fifo_login_submit" type="submit" value="Login"/>
				</p>
			</fieldset>
		</form>
	<?php
	return ob_get_clean();
}
// logs a member in after submitting a form
function fifo_login_member() {
 
	if(isset($_POST['fifo_user_login']) && wp_verify_nonce($_POST['fifo_login_nonce'], 'fifo-login-nonce')) {
 
		// this returns the user ID and other info from the user name
		$user = get_userdatabylogin($_POST['fifo_user_login']);
 
		if(!$user) {
			// if the user name doesn't exist
			fifo_errors()->add('empty_username', __('Invalid username'));
		}
 
		if(!isset($_POST['fifo_user_pass']) || $_POST['fifo_user_pass'] == '') {
			// if no password was entered
			fifo_errors()->add('empty_password', __('Please enter a password'));
		}
 
		// check the user's login with their password
		if(!wp_check_password($_POST['fifo_user_pass'], $user->user_pass, $user->ID)) {
			// if the password is incorrect for the specified user
			fifo_errors()->add('empty_password', __('Incorrect password'));
		}
 
		// retrieve all error messages
		$errors = fifo_errors()->get_error_messages();
 
		// only log the user in if there are no errors
		if(empty($errors)) {
 
			wp_setcookie($_POST['fifo_user_login'], $_POST['fifo_user_pass'], true);
			wp_set_current_user($user->ID, $_POST['fifo_user_login']);	
			do_action('wp_login', $_POST['fifo_user_login']);
 
			wp_redirect(home_url()); exit;
		}
	}
}
add_action('init', 'fifo_login_member');
// register a new user
function fifo_add_new_member() {
  	if (isset( $_POST["fifo_user_login"] ) && wp_verify_nonce($_POST['fifo_register_nonce'], 'fifo-register-nonce')) {
		$user_login		= $_POST["fifo_user_login"];	
		$user_email		= $_POST["fifo_user_email"];
		$user_first 	= $_POST["fifo_user_first"];
		$user_last	 	= $_POST["fifo_user_last"];
		$company_name	 	= $_POST["company_name"];
		$fifo_title	 	= $_POST["fifo_title"];
		$fifo_country	 	= $_POST["fifo_country"];						
		$user_pass		= $_POST["fifo_user_pass"];
		$pass_confirm 	= $_POST["fifo_user_pass_confirm"];
 
		// this is required for username checks
		require_once(ABSPATH . WPINC . '/registration.php');
 
		if(username_exists($user_login)) {
			// Username already registered
			fifo_errors()->add('username_unavailable', __('Username already taken'));
		}
		if(!validate_username($user_login)) {
			// invalid username
			fifo_errors()->add('username_invalid', __('Invalid username'));
		}
		if($user_login == '') {
			// empty username
			fifo_errors()->add('username_empty', __('Please enter a username'));
		}
		if(!is_email($user_email)) {
			//invalid email
			fifo_errors()->add('email_invalid', __('Invalid email'));
		}
		if(email_exists($user_email)) {
			//Email address already registered
			fifo_errors()->add('email_used', __('Email already registered'));
		}
		if($user_pass == '') {
			// passwords do not match
			fifo_errors()->add('password_empty', __('Please enter a password'));
		}
		if($user_pass != $pass_confirm) {
			// passwords do not match
			fifo_errors()->add('password_mismatch', __('Passwords do not match'));
		}
 
		$errors = fifo_errors()->get_error_messages();
 
		// only create the user in if there are no errors
		if(empty($errors)) {
 
			$new_user_id = wp_insert_user(array(
					'user_login'		=> $user_login,
					'user_pass'	 		=> $user_pass,
					'user_email'		=> $user_email,
					'first_name'		=> $user_first,
					'last_name'			=> $user_last,
					'user_registered'	=> date('Y-m-d H:i:s'),
					'role'				=> 'subscriber'
				)
			);
			if($new_user_id) {
				// send an email to the admin alerting them of the registration
				wp_new_user_notification($new_user_id);
 
 				//update custom wp user meta
				update_usermeta( $new_user_id, 'company_name', $company_name );
				update_usermeta( $new_user_id, 'fifo_title', $fifo_title );
				update_usermeta( $new_user_id, 'fifo_country', $fifo_country );

				// log the new user in
				wp_setcookie($user_login, $user_pass, true);
				wp_set_current_user($new_user_id, $user_login);	
				do_action('wp_login', $user_login);
 

//************** Stroing Data into another DB *********************
$db_username = "inspecta_cm";
$db_password = "a684008z";
$db_hostname = "localhost"; 

//connection to the database
$dbhandle = mysql_connect($db_hostname, $db_username, $db_password) 
 or die("Unable to connect to MySQL2");
//echo "Connected to MySQL<br>";

//select a database to work with
$selected = mysql_select_db("inspecta_cm",$dbhandle) 
  or die("Could not select DB2");

$pw= md5($user_pass); 

//execute the SQL query and return records
$result = mysql_query("INSERT INTO companies VALUES (
NULL , '".$company_name."', '', '', '', NULL, '', '', '', '', '', '".$fifo_country."', NULL, '', '', '', '', '', '', '', '', '', '', '".$user_email."', '', '', '', 'Registration', '6', '".date('Y-m-d')."'
)");
$company_id=mysql_insert_id();

mysql_query("INSERT INTO contact_persons VALUES (
NULL , '".$fifo_title."', '".$user_first."', '".$user_last."', '', '', '', '".$user_email."', '', '', '".$company_id."', NULL , 'Registration', '6', '".date('Y-m-d')."'
);
");
//mail send
// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
// More headers
$headers .= "From: <'".$user_email."'>" . "\r\n";
$to = "info@inspecta.biz";
$subject = "New Company Registration";

$txt = "Company Name:$company_name". "\r\n";
$txt .= "Contact Person:$fifo_title $user_first $user_last";
mail($to,$subject,$txt,$headers);
//close the connection
mysql_close($dbhandle);
				// send the newly created user to the home page after logging them in
				wp_redirect(home_url()); exit;
			}
 
		}
 
	}
}
add_action('init', 'fifo_add_new_member');
// used for tracking error messages
function fifo_errors(){
    static $wp_error; // Will hold global variable safely
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}
// displays error messages from form submissions
function fifo_show_error_messages() {
	if($codes = fifo_errors()->get_error_codes()) {
		echo '<div class="fifo_errors">';
		    // Loop error codes and display errors
		   foreach($codes as $code){
		        $message = fifo_errors()->get_error_message($code);
		        echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
		    }
		echo '</div>';
	}	
}
// register our form css
function fifo_register_css() {
	wp_register_style('fifo-form-css', plugin_dir_url( __FILE__ ) . '/css/forms.css');
}
add_action('init', 'fifo_register_css');
// load our form css
function fifo_print_css() {
	global $fifo_load_css;
 
	// this variable is set to TRUE if the short code is used on a page/post
	if ( ! $fifo_load_css )
		return; // this means that neither short code is present, so we get out of here
 
	wp_print_styles('fifo-form-css');
}
add_action('wp_footer', 'fifo_print_css');
